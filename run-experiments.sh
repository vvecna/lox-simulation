#!/bin/bash

# cd into the directory containing this script (from the bash faq 028)
if [[ $BASH_SOURCE = */* ]]; then
    cd -- "${BASH_SOURCE%/*}/" || exit 1
fi

# Check for the python dependencies we need at the end
./scripts/check-dependencies.py
if [ $? != 0 ]; then
    echo "Please make sure all dependencies are installed before running this script."
    exit 1
fi

# Number of simulation runs to do in parallel
parallel=""

# Number of simulation runs in each configuration
n=""

# Number of configurations in experiment 1
exp1=""
# Experiment 1 beginning
e1b=1
# Experiment 1 end
e1e=33

# Number of configurations in experiment 2
exp2=""
# Experiment 2 beginning
e2b=1
# Experiment 2 end
e2e=8

# Max observed memory use for 1 trial
mem_use=688

# Proceed without confirmation
noninteractive=false

# By default, run experiments and process results
run_experiments=true
process_results=true

# Get parameters
while getopts ":p:n:1:2:yer" opt; do
    case ${opt} in
        p)
            parallel="${OPTARG}"
            ;;
        n)
            n="${OPTARG}"
            ;;
        1)
            exp1="${OPTARG}"
            if [ "$exp1" != "" ]; then
                # If the user specified a range, use that range
                if [[ "$exp1" == *"-"* ]]; then
                    e1b="${exp1%%-*}"
                    e1e="${exp1##*-}"
                    exp1=$((e1e - e1b + 1))
                else
                    e1b=1
                    e1e="$exp1"
                fi
            fi
            ;;
        2)
            exp2="${OPTARG}"
            if [ "$exp2" != "" ]; then
                # If the user specified a range, use that range
                if [[ "$exp2" == *"-"* ]]; then
                    e2b="${exp2%%-*}"
                    e2e="${exp2##*-}"
                    exp2=$((e2e - e2b + 1))
                else
                    e2b=1
                    e2e="$exp2"
                fi
            fi
            ;;
        y)
            noninteractive=true
            ;;
        e)
            run_experiments=true
            process_results=false
            ;;
        r)
            run_experiments=false
            process_results=true
            ;;
    esac
done

# Run experiments unless -r flag was used
if [ "$run_experiments" == true ]; then

    # Ask user for values they didn't already specify

    if [ "$parallel" == "" ]; then
        read -e -p "How many simulation runs should we perform in parallel? (We suggest the number of CPU cores you have.) " parallel
        echo ""
    fi

    if [ "$n" == "" ]; then
        read -e -p "How many trials should we do in each configuration? [5] " n

        # Default to 5
        if [ "$n" == "" ]; then
            n=5
        fi
        echo ""
    fi

    if [ "$exp1" == "" ]; then
        read -e -p "How many configurations should we use in the first experiment? [33] " exp1

        # Default to 33 and max at 33
        if [[ "$exp1" == "" || "$exp1" -gt 33 ]]; then
            exp1=33
        # Min 0
        elif [[ "$exp1" -lt 0 ]]; then
            exp1=0
        fi
        echo ""

        # Begining and end
        e1b=1
        e1e="$exp1"
    fi

    if [ "$exp2" == "" ]; then
        read -e -p "How many configurations should we use in the second experiment? [8] " exp2

        # Default to 10 and max at 10
        if [[ "$exp2" == "" || "$exp2" -gt 8 ]]; then
            exp2=8
        # Min 0
        elif [[ "$exp2" -lt 0 ]]; then
            exp2=0
        fi
        echo ""

        # Beginning and end
        e2b=1
        e2e="$exp2"
    fi

    num_configs=$((exp1 + exp2))
    num_trials=$((num_configs * n))
    batches=$(( (num_trials + parallel - 1) / parallel))

    if [[ "$parallel" -gt "$num_trials" ]]; then
        parallel=$num_trials
    fi

    echo "We will test Troll Patrol in ${num_configs} configurations."
    echo "We will run the simulation ${n} times in each configuration."
    echo "This results in a total of ${num_trials} simulation runs."
    echo "We will do ${parallel} runs in parallel, so this is ${batches} batches."
    echo "It is recommended that you have at least ${parallel} CPU cores and $((parallel * mem_use)) MB of RAM."
    echo "If you don't have enough cores or RAM, try reducing the number of parallel simulation runs."
    echo "It is anticipated that this will take from $((batches + batches / 2)) to $((batches * 2)) days to complete."
    if [ "$noninteractive" == false ]; then
        read -e -p "Is this okay? (y/N) " res
        if [[ "$res" != "Y" && "$res" != "y" ]]; then
            exit 1
        fi
    fi

    ./scripts/run-experiments.sh "$parallel" "$n" "$e1b" "$e1e" "$e2b" "$e2e"
fi

# Process results unless -e flag was used
if [ "$process_results" == true ]; then

    # Parse out bridge info we want to plot
    for i in results/*/*-simulation; do
        sed -n '/^Full stats per bridge:$/,/^End full stats per bridge$/{//!p;}' \
            "$i" > "${i%-simulation}-bridges.csv"
        sim_begin=$(grep -Po '(?<=Simulation began on day )(.*)(?=$)' "$i")
        censor_begin=$(grep -Po '(?<=Censor began on day )(.*)(?=$)' "$i")
        echo "$sim_begin,$censor_begin" > "${i%-simulation}-start.csv"
    done

    if [ "$n" == "" ]; then
        read -e -p "How many trials did we do in each configuration? [5] " n

        # Default to 5
        if [ "$n" == "" ]; then
            n=5
        fi
        echo ""
    fi

    ./scripts/plot-results.py 1 $n results/1/*-bridges.csv
    ./scripts/plot-results.py 2 $n results/2/*-bridges.csv
fi

echo "Done. See the results directory for the output."
