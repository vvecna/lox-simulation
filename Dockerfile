FROM ubuntu:24.04

# Default directory
WORKDIR /home/user
COPY setup_files/* /home/user/build/
RUN sed -i 's@HOMEDIR@/home/user@g' /home/user/build/config.toml

RUN apt update -y

# git for cloning repos
# cargo for building everything except rdsys
# golang and make for building rdsys
# openssl and libssl-dev as dependencies for (building) the software
#
# psmisc for killing processes after simulation completes
#   (killall -s INT rdsys lox-distributor troll-patrol)
# time for collecting performance stats on rdsys
#   (other code uses a Rust crate for this)
RUN apt install -y git cargo golang make openssl libssl-dev psmisc time

RUN mkdir -p /home/user/build /home/user/rdsys /home/user/lox-distributor /home/user/troll-patrol /home/user/simulation

# rdsys

# Clone rdsys
WORKDIR /home/user/build
RUN git clone https://gitlab.torproject.org/tpo/anti-censorship/rdsys.git
WORKDIR /home/user/build/rdsys
RUN git checkout 79332a3ee69dc6022a2a29fd8b79e9d2e4f5c9ab
RUN sed -i 's/NUM_BRIDGES = 1000/NUM_BRIDGES = 3600/' scripts/mkdescriptors/main.go
# configure rdsys to give Lox all the bridges
RUN cp /home/user/build/config.json conf/

# Build rdsys and bridge lines
WORKDIR /home/user/build/rdsys
RUN make build && make descriptors
RUN cp -r conf descriptors /home/user/rdsys/
# Copy backend executable as "rdsys"
RUN cp backend /home/user/rdsys/rdsys


# Clone and build things that depend on each other together

# Lox distributor
WORKDIR /home/user/build
RUN git clone https://gitlab.torproject.org/vecna/lox.git
WORKDIR /home/user/build/lox
RUN git checkout 5adf9ee7f070f9909f09ab9342f84376d11dd7dc
RUN mkdir -p .cargo
RUN cp /home/user/build/config.toml .cargo/

# Troll Patrol
WORKDIR /home/user/build
RUN git clone https://git-crysp.uwaterloo.ca/vvecna/troll-patrol.git
WORKDIR /home/user/build/troll-patrol
RUN git checkout 7acba0a6f00c6ffdb429b4993ee109a8e125b466
RUN mkdir -p .cargo
RUN cp /home/user/build/config.toml .cargo/

# lox_cli
WORKDIR /home/user/build
RUN git clone https://git-crysp.uwaterloo.ca/vvecna/lox_cli.git
WORKDIR /home/user/build/lox_cli
RUN git checkout d7beaad5601ad309fd5936c19e60a7ea98a05fde
RUN mkdir -p .cargo
RUN cp /home/user/build/config.toml .cargo/

# Build Lox distributor
WORKDIR /home/user/build/lox/crates/lox-distributor
RUN cargo update && cargo build --release --features simulation
RUN cp config.json /home/user/build/lox/target/release/lox-distributor /home/user/lox-distributor/

# Build Troll Patrol
WORKDIR /home/user/build/troll-patrol
RUN cargo update && cargo build --release --features simulation
RUN cp target/release/troll-patrol /home/user/troll-patrol/


# Lox Simulation
WORKDIR /home/user/build/lox-simulation
COPY Cargo.toml /home/user/build/lox-simulation/
RUN mkdir src
COPY src/* /home/user/build/lox-simulation/src/
RUN mkdir -p .cargo
RUN cp /home/user/build/config.toml .cargo/

# Build simulation
WORKDIR /home/user/build/lox-simulation
RUN cargo update && cargo build --release
RUN cp target/release/lox-simulation /home/user/simulation/simulation

WORKDIR /home/user
