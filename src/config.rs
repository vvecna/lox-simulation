use crate::censor;

use lox_cli::networking::*;
use lox_library::IssuerPubKey;

pub struct Config {
    pub la_pubkeys: Vec<IssuerPubKey>,
    pub la_net: HyperNet,
    pub tp_net: HyperNet,
    pub bootstrapping_period_duration: u32,
    // Define censor behavior
    pub censor_secrecy: censor::Secrecy,
    // The maximum number of connections for the censor to make to each
    // bridge
    pub censor_max_connections: u32,
    // The maximum number of positive reports for the censor to submit
    pub censor_max_pr: u32,
    pub censor_speed: censor::Speed,
    pub censor_event_duration: u32,
    pub censor_totality: censor::Totality,
    pub censor_partial_blocking_percent: f64,
    // We model only one country at a time because Lox assumes censors
    // share information with each other.
    pub country: String,
    pub one_positive_report_per_cred: bool,
    // Probability that a censor-cooperating user can convince an honest
    // user to give them an invite. This number is the baseline, for
    // soliciting an invitation from a level 1 user. We assume it is
    // harder for the censor to get invitations from higher-level users.
    pub prob_censor_gets_invite: f64,
    // Probability that a connection randomly fails, even though censor
    // does not block the bridge
    pub prob_connection_fails: f64,
    // If the connection fails, retry how many times?
    pub num_connection_retries: u32,
    pub prob_user_connects: f64,
    pub prob_user_invites_friend: f64,
    pub prob_user_submits_reports: f64,
    pub prob_user_treats_throttling_as_blocking: f64,
}
