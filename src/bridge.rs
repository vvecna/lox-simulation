use lox_library::bridge_table::BridgeLine;
use std::collections::BTreeMap;
use troll_patrol::{extra_info::ExtraInfo, get_date};

// The Bridge struct only tracks data for today
pub struct Bridge {
    // This is the hashed fingerprint
    pub fingerprint: [u8; 20],

    // The following four values are Julian dates used to track
    // accuracy of the Troll Patrol system. A value of 0 is used to
    // indicate this event *has not happened yet*. Note that 0 is not a
    // date we will ever encounter.

    // Date the bridge was first distributed to a user, i.e., the date
    // we created this Bridge object
    pub first_distributed: u32,

    // Date the bridge was first distributed to a non-censor user
    pub first_real_user: u32,

    // First date a censor blocked this bridge
    pub first_blocked: u32,

    // First date Troll Patrol detected that this bridge was blocked
    // (whether or not it was correct)
    pub first_detected_blocked: u32,

    // First date Troll Patrol received a positive report for this
    // bridge (for identifying stage three)
    pub first_positive_report: u32,

    pub real_connections: u32,
    pub total_connections: u32,
}

impl Bridge {
    pub fn new(fingerprint: &[u8; 20]) -> Self {
        Self {
            fingerprint: *fingerprint,
            first_distributed: get_date(),
            first_real_user: 0, // set this afterwards if user is non-censor
            first_blocked: 0,
            first_detected_blocked: 0,
            first_positive_report: 0,
            real_connections: 0,
            total_connections: 0,
        }
    }

    pub fn from_bridge_line(bridgeline: &BridgeLine) -> Self {
        Self::new(&bridgeline.get_hashed_fingerprint())
    }

    pub fn connect_real(&mut self) {
        self.real_connections += 1;
        self.total_connections += 1;
    }

    pub fn connect_total(&mut self) {
        self.total_connections += 1;
    }

    // Let the censor simulate a bunch of connections at once
    pub fn censor_flood(&mut self, num_connections: u32) {
        self.total_connections += num_connections;
    }

    // Generate an extra-info report for today
    pub fn gen_extra_info(&self, country: &str) -> ExtraInfo {
        let mut bridge_ips = BTreeMap::<String, u32>::new();
        // Round up to a multiple of 8
        let rounded_connection_count =
            self.total_connections + 7 - (self.total_connections + 7) % 8;
        //let rounded_connection_count = (self.total_connections + 7) / 8 * 8;
        bridge_ips.insert(country.to_string(), rounded_connection_count);
        ExtraInfo {
            nickname: String::from("simulation-bridge"),
            fingerprint: self.fingerprint,
            date: get_date(),
            bridge_ips,
        }
    }

    pub fn reset_for_tomorrow(&mut self) {
        self.real_connections = 0;
        self.total_connections = 0;
    }

    // Has this bridge been distributed to a non-censor user?
    pub fn has_been_distributed(&self) -> bool {
        self.first_real_user > 0
    }

    // Does Troll Patrol think this bridge is blocked?
    pub fn troll_patrol_blocked(&self) -> bool {
        self.first_detected_blocked > 0
    }
}
