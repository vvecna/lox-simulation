// Before running this, run:
// 1. rdsys
// 2. lox-distributor
// 3. troll-patrol with the feature "simulation"

use lox_simulation::{
    bridge::Bridge,
    censor::{self, Censor},
    config::Config as SConfig,
    extra_infos_server,
    user::User,
};

use clap::Parser;
use lox_cli::{networking::*, *};
use lox_library::cred::Invitation;
use memory_stats::memory_stats;
use rand::{prelude::SliceRandom, Rng};
use serde::Deserialize;
use std::{
    collections::{HashMap, HashSet},
    fs::File,
    io::BufReader,
    path::PathBuf,
    time::Duration,
};
use tokio::{spawn, time::sleep};
use troll_patrol::{extra_info::ExtraInfo, get_date, increment_simulated_date};

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    /// Name/path of the configuration file
    #[arg(short, long, default_value = "simulation_config.json")]
    config: PathBuf,
}

#[derive(Debug, Deserialize)]
pub struct Config {
    pub la_port: u16,
    pub la_test_port: u16,
    pub tp_port: u16,
    pub tp_test_port: u16,
    pub bootstrapping_period_duration: u32,
    pub censor_secrecy: censor::Secrecy,
    pub censor_max_connections: u32,
    pub censor_max_pr: u32,
    pub censor_speed: censor::Speed,
    pub censor_event_duration: u32,
    pub censor_totality: censor::Totality,
    pub censor_partial_blocking_percent: f64,
    pub country: String,
    pub min_new_users_per_day: u32,
    pub max_new_users_per_day: u32,
    pub num_connection_retries: u32,
    // How many days to simulate
    pub num_days: u32,
    pub one_positive_report_per_cred: bool,
    pub prob_censor_gets_invite: f64,
    pub prob_connection_fails: f64,
    pub prob_user_connects: f64,
    pub prob_user_invites_friend: f64,
    pub prob_user_submits_reports: f64,
    pub prob_user_treats_throttling_as_blocking: f64,
}

#[tokio::main]
pub async fn main() {
    let args: Args = Args::parse();

    let config: Config = serde_json::from_reader(BufReader::new(
        File::open(&args.config).expect("Could not read config file"),
    ))
    .expect("Reading config file from JSON failed");

    let la_net = HyperNet {
        hostname: format!("http://localhost:{}", config.la_port),
    };
    let la_net_test = HyperNet {
        hostname: format!("http://localhost:{}", config.la_test_port),
    };
    let tp_net = HyperNet {
        hostname: format!("http://localhost:{}", config.tp_port),
    };
    let tp_net_test = HyperNet {
        hostname: format!("http://localhost:{}", config.tp_test_port),
    };
    let extra_infos_net = HyperNet {
        hostname: "http://localhost:8004".to_string(),
    };

    let la_pubkeys = get_lox_auth_keys(&la_net).await.unwrap();

    let sconfig = SConfig {
        la_pubkeys,
        la_net,
        tp_net,
        bootstrapping_period_duration: config.bootstrapping_period_duration,
        censor_secrecy: config.censor_secrecy,
        censor_max_connections: config.censor_max_connections,
        censor_max_pr: config.censor_max_pr,
        censor_speed: config.censor_speed,
        censor_event_duration: config.censor_event_duration,
        censor_totality: config.censor_totality,
        censor_partial_blocking_percent: config.censor_partial_blocking_percent,
        country: config.country,
        num_connection_retries: config.num_connection_retries,
        one_positive_report_per_cred: config.one_positive_report_per_cred,
        prob_censor_gets_invite: config.prob_censor_gets_invite,
        prob_connection_fails: config.prob_connection_fails,
        prob_user_connects: config.prob_user_connects,
        prob_user_invites_friend: config.prob_user_invites_friend,
        prob_user_submits_reports: config.prob_user_submits_reports,
        prob_user_treats_throttling_as_blocking: config.prob_user_treats_throttling_as_blocking,
    };

    let mut rng = rand::thread_rng();

    // Set up censor
    let mut censor = Censor::new(&sconfig);

    // Set up bridges (no bridges yet)
    let mut bridges = HashMap::<[u8; 20], Bridge>::new();

    // Set up users
    let mut users = Vec::<User>::new();

    // We have some pool of invitations available
    let mut invites = Vec::<Invitation>::new();

    // Set up extra-infos server
    spawn(async move {
        extra_infos_server::server().await;
    });
    sleep(Duration::from_millis(1)).await;

    // Only consider bridges that have been distributed to users
    let mut false_neg = 0;
    let mut false_pos = 0;
    let mut true_neg = 0;
    let mut true_pos = 0;

    // All bridges, including those only known to the censor
    let mut total_fn = 0;
    let mut total_fp = 0;
    let mut total_tn = 0;
    let mut total_tp = 0;

    // Get starting date
    let start_date = get_date();

    // Track daily percentage of users who have at least one working bridge
    let mut percent_users_can_connect = Vec::<f64>::new();

    // Track memory use during simulation
    let mut max_physical_mem = 0;
    let mut max_virtual_mem = 0;

    // We maintain a backlog of users trying to connect
    let mut num_users_requesting_invites = 0;

    // Main loop
    for day in 1..=config.num_days {
        // Save some function calls by storing this
        let date = get_date();

        // Count of users who could use at least one bridge today
        let mut count_users_can_connect = 0;
        let mut count_users_cannot_connect = 0;

        println!("Starting day {} of the simulation", day);
        println!(
            "    {} prospective users want to join",
            num_users_requesting_invites
        );
        println!(
            "    We have {} users and {} bridges",
            users.len(),
            bridges.len()
        );
        println!(
            "    The censor has learned {} bridges",
            censor.known_bridges.len()
        );
        println!("    Accuracy thus far:");
        println!("        True Positives: {}", true_pos);
        println!("        True Negatives: {}", true_neg);
        println!("        False Positives: {}", false_pos);
        println!("        False Negatives: {}", false_neg);

        if let Some(usage) = memory_stats() {
            if usage.physical_mem > max_physical_mem {
                max_physical_mem = usage.physical_mem;
            }
            if usage.virtual_mem > max_virtual_mem {
                max_virtual_mem = usage.virtual_mem;
            }
        } else {
            println!("Failed to get the current memory usage");
        }

        // USER TASKS

        // Add number of users who want to join today
        num_users_requesting_invites +=
            rng.gen_range(config.min_new_users_per_day..=config.max_new_users_per_day);

        let mut new_users = Vec::<User>::new();

        // Shuffle users so they act in a random order
        users.shuffle(&mut rng);

        // Users do daily actions
        let mut i = 0;
        while i < users.len() {
            let user = users.get_mut(i).unwrap();
            if let Ok((mut invited_friends, remove_user)) = user
                .daily_tasks(&sconfig, &mut bridges, &mut censor, &mut invites)
                .await
            {
                // We remove censor users once they stop serving a purpose
                if remove_user {
                    // This removes the user and replaces them with the
                    // last element of the vector, for efficiency. This
                    // is fine to do because the users act in a random
                    // order anyway.
                    users.swap_remove(i);
                    continue;
                }

                if invited_friends.len() > 0 {
                    new_users.append(&mut invited_friends);
                }
            }

            // Count the number of non-censor users who were able to
            // connect to at least one bridge today
            if !user.is_censor && user.attempted_connection {
                if user.able_to_connect {
                    count_users_can_connect += 1;
                } else {
                    count_users_cannot_connect += 1;
                }
            }

            // Iterate loop (note that we do not reach this if we remove
            // a user, so we'll get the replacement user at that same
            // index)
            i += 1;
        }

        // Also count number of new users with/without connections
        for user in &new_users {
            // Count the number of non-censor users who are able to
            // connect to at least one bridge
            if !user.is_censor {
                if user.able_to_connect {
                    count_users_can_connect += 1;
                } else {
                    count_users_cannot_connect += 1;
                }
            }
        }

        // Add percent of users who can connect to vector
        let percent_connect_today = count_users_can_connect as f64
            / (count_users_can_connect + count_users_cannot_connect) as f64;
        println!(
            "    Percent of users who can connect today: {}",
            percent_connect_today
        );
        percent_users_can_connect.push(percent_connect_today);

        // Add new users
        users.append(&mut new_users);

        // Track new user count
        let mut num_new_users = 0;

        // For each user requesting an invite, see if one is available.
        // If not, they can try to join via open-entry invitation if the
        // censor is not active.
        for _ in 0..num_users_requesting_invites {
            let mut user_created = false;

            // Try invites until one works or we run out
            while let Some(invite) = invites.pop() {
                if let Ok(user) =
                    User::from_invite(invite, &sconfig, false, &mut bridges, &mut censor).await
                {
                    users.push(user);
                    user_created = true;

                    // We got a user. Stop now.
                    break;
                }
            }

            // If we couldn't get a working invite, try open-entry invite
            if !user_created && !censor.is_active() {
                if let Ok(user) = User::new(&sconfig, false, &mut bridges, &mut censor).await {
                    users.push(user);
                    user_created = true;
                }
            }

            if user_created {
                // We successfully added a user, increment count
                num_new_users += 1;
            }
        }

        // Remove new users from backlog
        num_users_requesting_invites -= num_new_users;

        // CENSOR TASKS

        // On the day the censor activates, get as many credentials as
        // possible for the current bridge. For efficiency, don't get
        // credentials for bridges that will never be distributed to
        // non-censor users.
        if date == censor.start_date {
            let num_bridges_before = censor.known_bridges.len();

            // Censor gets as many invites as possible for 1 bridge
            while let Ok(new_user) = User::new(&sconfig, true, &mut bridges, &mut censor).await {
                // Add new censor user
                users.push(new_user);

                // If we now know 2 more bridges, break
                if censor.known_bridges.len() > num_bridges_before + 1 {
                    break;
                }
            }
        }

        if censor.is_active() {
            censor.end_of_day_tasks(&sconfig, &mut bridges).await;
        }

        // BRIDGE TASKS
        let mut new_extra_infos = HashSet::<ExtraInfo>::new();
        for (_, bridge) in bridges.iter_mut() {
            // Bridge reports its connections for the day
            new_extra_infos.insert(bridge.gen_extra_info(&sconfig.country));

            // Bridge resets for tomorrow
            bridge.reset_for_tomorrow();
        }

        // Publish all the bridges' extra-infos for today
        let result = extra_infos_net
            .request(
                "/add".to_string(),
                serde_json::to_string(&new_extra_infos).unwrap().into(),
            )
            .await;
        if result.is_ok() {
            result.unwrap();
        } else {
            eprintln!("Failed to publish new extra-infos");
        }

        // TROLL PATROL TASKS
        let new_blockages_resp = tp_net_test.request("/update".to_string(), vec![]).await;
        let new_blockages = match new_blockages_resp {
            Ok(resp) => match serde_json::from_slice(&resp) {
                Ok(new_blockages) => new_blockages,
                Err(e) => {
                    eprintln!("Failed to deserialize new blockages, error: {}", e);
                    HashMap::<String, HashSet<String>>::new()
                }
            },
            Err(e) => {
                eprintln!(
                    "Failed to get new blockages from Troll Patrol, error: {}",
                    e
                );
                HashMap::<String, HashSet<String>>::new()
            }
        };

        // Since we have only one censor, just convert to a set of bridges
        let mut blocked_bridges = HashSet::<[u8; 20]>::new();
        for (bridge, ccs) in new_blockages {
            let fingerprint = array_bytes::hex2array(bridge).unwrap();
            if ccs.contains(&sconfig.country) {
                blocked_bridges.insert(fingerprint);
            }
        }

        for (fingerprint, bridge) in &mut bridges {
            let detected_blocked = blocked_bridges.contains(fingerprint);

            // If this is the first day Troll Patrol has determined this
            // bridge is blocked, note that for stats
            if detected_blocked && bridge.first_detected_blocked == 0 {
                bridge.first_detected_blocked = date;
            }

            // Check if censor actually blocks this bridge
            let really_blocked = censor.blocks_bridge(&sconfig, fingerprint);
            if really_blocked && bridge.first_blocked == 0 {
                bridge.first_blocked = date;
            }

            // Increase appropriate count. Only increase main count if
            // this is a bridge that has actually been distributed to a
            // non-censor user. Increase the total count regardless.
            if detected_blocked && really_blocked {
                if bridge.first_real_user > 0 {
                    true_pos += 1;
                }
                total_tp += 1;
            } else if detected_blocked {
                if bridge.first_real_user > 0 {
                    false_pos += 1;
                }
                total_fp += 1;
            } else if really_blocked {
                if bridge.first_real_user > 0 {
                    false_neg += 1;
                }
                total_fn += 1;
            } else {
                if bridge.first_real_user > 0 {
                    true_neg += 1;
                }
                total_tn += 1;
            }
        }

        // LOX AUTHORITY TASKS

        // Advance LA's time to tomorrow
        let result = la_net_test
            .request(
                "/advancedays".to_string(),
                serde_json::to_string(&(1 as u16)).unwrap().into(),
            )
            .await;
        if result.is_ok() {
            result.unwrap();
        } else {
            eprintln!("Failed to advance time for LA");
        }

        // SIMULATION TASKS

        // Advance simulated time to tomorrow
        increment_simulated_date();
    }

    // Print various information about the simulation run
    println!(
        "\nSimulation ended with {} users and {} bridges",
        users.len(),
        bridges.len()
    );
    println!("The censor learned {} bridges", censor.known_bridges.len());

    println!(
        "\nMaximum physical memory usage during simulation: {}",
        max_physical_mem
    );
    println!(
        "Maximum virtual memory usage during simulation: {}\n",
        max_virtual_mem
    );

    println!("\nThese total values include bridges never distributed to real users...");
    println!("Total true positives: {}", total_tp);
    println!("Total true negatives: {}", total_tn);
    println!("Total false positives: {}", total_fp);
    println!("Total false negatives: {}", total_fn);

    println!("\nThese values only include bridges actually distributed to users...");
    println!("True Positives: {}", true_pos);
    println!("True Negatives: {}", true_neg);
    println!("False Positives: {}", false_pos);
    println!("False Negatives: {}", false_neg);

    println!("\nFull stats per bridge:");

    println!(
        "Fingerprint,first_distributed,first_real_user,first_blocked,first_detected_blocked,first_positive_report"
    );
    for (fingerprint, bridge) in bridges {
        println!(
            "{},{},{},{},{},{}",
            array_bytes::bytes2hex("", fingerprint),
            bridge.first_distributed,
            bridge.first_real_user,
            bridge.first_blocked,
            bridge.first_detected_blocked,
            bridge.first_positive_report
        );
    }
    println!("End full stats per bridge\n");

    println!("\nWhich users can connect:");
    println!("join_date,able_to_connect");
    for user in users {
        if !user.is_censor {
            println!("{},{}", user.join_date, user.able_to_connect);
        }
    }
    println!("End which users can connect");

    println!("\nSimulation began on day {}", start_date);
    println!("Censor began on day {}\n", censor.start_date);

    println!("\nPercent of users who can connect per day:");
    println!("date,percent");
    for i in 0..percent_users_can_connect.len() {
        println!("{},{}", start_date + i as u32, percent_users_can_connect[i]);
    }
    println!("End percent of users who can connect per day");

    println!(
        "\n{} users were unable to join at all.",
        num_users_requesting_invites
    );
}
