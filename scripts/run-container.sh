#!/bin/bash

img="troll-patrol"
exp_num="$1"
uuid="$2"

container=$(docker run --rm -d -i $img)

# Create results directory if it doesn't already exist
mkdir -p results/${exp_num}

docker cp configs/${uuid}/troll_patrol_config.json $container:/home/user/troll-patrol/
cat configs/${uuid}/troll_patrol_config.json >> "results/${exp_num}/${uuid}"-troll_patrol_config.json
docker cp configs/${uuid}/simulation_config.json $container:/home/user/simulation/
cat configs/${uuid}/simulation_config.json >> "results/${exp_num}/${uuid}"-simulation_config.json

# Run rdsys to give bridges to LA
docker exec $container sh \
    -c "cd /home/user/rdsys/ && /usr/bin/time -v ./rdsys -config conf/config.json" \
    2>&1 | grep -P '\t' > "results/${exp_num}/${uuid}"-rdsys &

# Give rdsys time to start up
sleep 5

# Run LA, filtering a lot of the output because we don't need it
docker exec $container sh \
    -c "cd /home/user/lox-distributor/ && ./lox-distributor" \
    | grep -v BridgeLine \
    | grep -v "Added bridge with fingerprint" \
    | grep -v "Today's date according to server" \
    &> "results/${exp_num}/${uuid}"-lox-distributor &

# Give LA time to start up
sleep 5

# Run Troll Patrol
docker exec $container sh \
    -c "cd /home/user/troll-patrol/ && ./troll-patrol --config troll_patrol_config.json" \
    &> "results/${exp_num}/${uuid}"-troll-patrol &

# Give Troll Patrol time to start up
sleep 5

# Run simulation and then kill other processes
docker exec $container sh \
    -c "cd /home/user/simulation/ && ./simulation && killall -s INT rdsys lox-distributor troll-patrol" \
    | tee "results/${exp_num}/${uuid}"-simulation 2>&1

# Stop the container once it's done
echo "Stopping docker container... It should be removed."
docker stop $container
