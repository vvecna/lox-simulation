#!/bin/bash

# Generate the configuration files for an experiment we're about to do

exp_num="$1"
secrecy="$2"
harshness="$3"
prob="$4"
uuid="$5"

mkdir -p "configs/${uuid}"
f1="configs/${uuid}/troll_patrol_config.json"
cp "configs/troll_patrol_config.json.template" "${f1}"
f2="configs/${uuid}/simulation_config.json"
cp "configs/simulation_config.json.template" "${f2}"

# Troll Patrol config
sed -i "s/HARSHNESS/$harshness/" "${f1}"

# Lox Simulation config
sed -i "s/CENSOR_SECRECY/\"$secrecy\"/" "${f2}"
sed -i "s/PROB_USER_SUBMITS_REPORTS/$prob/" "${f2}"
