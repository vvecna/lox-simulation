#!/bin/bash

# Number to run in parallel
parallel="$1"

# Number of runs in each configuration
n="$2"

# First and last configuration of experiment 1
e1b="$3"
e1e="$4"

# First and last configuration of experiment 2
e2b="$5"
e2e="$6"

# Build docker container
docker build -t troll-patrol .

# Parameters should be:
# $1: experiment number (1 or 2)
# $2: censor secrecy (Overt or Flooding)
# $3: harshness (0-4)
# $4: probability of users submitting reports (0.0-1.0)
run_docker() {
    # Get a UUID so each simulation run stores its output in a different file
    uuid=$(cat /proc/sys/kernel/random/uuid)

    ./scripts/gen-configs.sh $1 $2 $3 $4 $uuid
    ./scripts/run-container.sh $1 $uuid

    # If harshness = 2, probability of users submitting reports=0.25,
    # experiment number = 1, then copy the results to experiment 2
    # directory.
    if [[ "$3" == 2 && "$4" == 0.25 && "$1" == 1 ]]; then
        mkdir -p results/2
        cp results/1/${uuid}-* results/2/
    fi
}

# Make list of all configurations to use
configs=()

# Experiment 1
for i in $(seq $e1b $e1e); do
    line=$(sed -n "${i}p" configs/experiment-1)
    for j in $(seq $n); do
        configs+=( "1 $line" )
    done
done

# Experiment 2
for i in $(seq $e2b $e2e); do
    line=$(sed -n "${i}p" configs/experiment-2)
    for j in $(seq $n); do
        configs+=( "2 $line" )
    done
done

# Go through configs in batches of $n
index=0
while [[ $index -lt ${#configs[@]} ]]; do
    # Note: Elements contain multiple tokens. Don't put this in quotes.
    run_docker ${configs[$index]} &

    index=$((index + 1))

    if [[ $(($index % parallel)) == 0 ]]; then
        # Finish this batch before starting the next one
        wait
    fi
done

# Finish the final batch
wait
